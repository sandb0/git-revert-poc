function latamOperations() {
    console.log('Initial LATAM operations...');
}

(function main() {
    const fruits = ['banana', 'apples', 'watermelon'];

    const fruit = 'banana';

    if (fruits.includes(fruit)) {
        console.log('Yes! Have banana!');
    } else if (fruits.includes('apples')) {
        console.log('Yes! Have apple!');
    }
})();
